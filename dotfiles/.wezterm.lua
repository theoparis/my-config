local wezterm = require('wezterm')

return {
	default_prog = { '/bin/zsh' },
	font = wezterm.font('ComicShannsMono Nerd Font'),
	font_size = 18,
	window_background_opacity = 0.75,
}
