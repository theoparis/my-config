# Theo's Config Scripts

## Requirements

- fish
- git 
- minizip-ng
- stow

## Initialization

Anyways, first thing you should do is clone the repo like so:

```bash
git clone https://codeberg.org/theoparis/my-config ~/my-config
```

Next, run the init script that will link the config files from the local git repository to the respective config folders.

```
cd ~/my-config
./scripts/init.fish
```

