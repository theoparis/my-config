# Theo's Setup

## Operating System 

I use NixOS as my daily driver, along with [the Hyprland compositor](https://github.com/hyprwm/hyprland). For my terminal, I use the gpu-accelerated [rio terminal](https://github.com/raphamorim/rio) with the [ComicShannsMono Nerd Font](https://github.com/ryanoasis/nerd-fonts/). [I am also using nushell - here is my configuration file.](dotfiles/.config/nushell/config.nu)

## Neovim

[You can my lua configuration file here](dotfiles/.config/nvim/init.lua).

